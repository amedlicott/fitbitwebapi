package com.digitalml.service;

import static spark.Spark.*;
import spark.*;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.MultipartBody;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Context.Builder;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.context.JavaBeanValueResolver;
import com.github.jknack.handlebars.context.MapValueResolver;

import static net.logstash.logback.argument.StructuredArguments.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class FitbitWebAPIRouting {

    private static final Logger logger = LoggerFactory.getLogger("fitbitwebapi:1");

    private static final String cacheURL = "http://infocache-test:9200/fitbitwebapi-1";
    
    private static final String resourceName = "fitbitwebapi";

    public static void main(String[] args) {
   
   		// Initialise index on startup
		Unirest.put(cacheURL).asStringAsync();
   
        port(4567);
    
        get("/ping", (req, res) -> {
            return "pong";
        });
        
        get("/halt", (request, response) -> {
			stop();
			response.status(202);
			return "";
		});
		
        // Handle timings
        
        Map<Object, Long> timings = new ConcurrentHashMap<>();
        
        before(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.put(request, System.nanoTime());
        	}
        });
        
        after(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		long start = timings.remove(request);
        		long end =  System.nanoTime();
        		logger.info("log message {} {} {} {} ns", value("apiname", "fitbitwebapi"), value("apiversion", "1"), value("apipath", request.pathInfo()), value("response-timing", (end-start)));
        	}
        });
        
        afterAfter(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.remove(request);
        	}
        });

        get("/1/foods/:food-id.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/body/log/weight.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/:collection-path/apiSubscriptions.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/tracker/:resource-path/date/:base-date/:end-date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/foods.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/1/user/-/meals/:meal-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/1/user/-/meals/:meal-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/1/user/-/activities/favorite/:activity-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/1/user/-/activities/favorite/:activity-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/1/user/-/body/log/fat/:body-fat-log-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/activities/tracker/:resource-path/date/:date/:period.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/friends/invitations.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/heart/date/:date/:end-date/:detail-level.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/oauth2/token", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/activities/favorite.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/body/log/fat/date/:date/:period.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/foods/log/:resource-path/date/:date/:period.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/body/log/fat/goal.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/activities/:resource-path/date/:base-date/:end-date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/foods/log/water/goal.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/foods/log/water/goal.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/foods/search.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/heart/date/:date/1d/:detail-level.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/date/:date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/1/user/-/activities/:activity-log-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/1/user/-/foods/log/:food-log-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/1/user/-/foods/log/favorite/:food-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/1/user/-/foods/log/favorite/:food-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/activities/:resource-path/date/:date/:period.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/1.2/user/-/sleep/:log-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/sleep/:resource-path/date/:date/:period.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/body/log/:goal-type/goal.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1.2/user/-/sleep/list.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/recent.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/activities/:activity-id.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/devices.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/foods/log/frequent.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/1/user/-/body/log/weight/:body-weight-log-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/profile.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/profile.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/body/log/weight/date/:date/:period.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/body/log/weight/date/:date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/foods/log/water/date/:date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1.2/user/-/sleep.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/devices/tracker/:tracker-id/alarms.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/devices/tracker/:tracker-id/alarms.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/activities/frequent.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/heart/date/:date/:end-date/:detail-level/time/:start-time/:end-time.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/body/log/weight/date/:base-date/:end-date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/heart/date/:date/:period.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/body/log/fat/date/:date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/devices/tracker/:tracker-id/alarms/:alarm-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/1/user/-/devices/tracker/:tracker-id/alarms/:alarm-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/activities/goals/:period.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/activities/goals/:period.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/activities/:resource-path/date/:date/:end-date/:detail-level/time/:start-time/:end-time.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/body/:resource-path/date/:base-date/:end-date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/foods/log/date/:date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/:resource-path/date/:date/1d/:detail-level/time/:start-time/:end-time.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/sleep/:resource-path/date/:base-date/:end-date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1.2/user/-/sleep/date/:date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/body/:resource-path/date/:date/:period.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/foods/locales.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/foods/log/water/:water-log-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/1/user/-/foods/log/water/:water-log-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/1/user/-/:collection-path/apiSubscriptions/:subscription-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/1/user/-/:collection-path/apiSubscriptions/:subscription-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/friends/leaderboard.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/:resource-path/date/:base-date/:end-date/:detail-level.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/list.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/body/log/fat.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/activities/heart/date/:date/1d/:detail-level/time/:start-time/:end-time.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/heart/date/:base-date/:end-date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/body/log/weight/goal.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/body/log/fat/date/:base-date/:end-date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/foods/log.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1.2/user/-/sleep/date/:base-date/:end-date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1.2/user/-/sleep/goal.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1.2/user/-/sleep/goal.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/activities.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/activities.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/foods/units.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/:resource-path/date/:date/1d/:detail-level.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/activities.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/foods/log/water.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/meals.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/meals.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/foods/log/recent.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/activities/:log-id.tcx", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/foods/log/:resource-path/date/:base-date/:end-date.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/1/user/-/foods/:food-id.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/foods/log/favorite.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/foods/log/goal.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/1/user/-/foods/log/goal.json", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/1/user/-/friends.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/friends/invitations/:from-user-id.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/1/user/-/badges.json", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
    }
}